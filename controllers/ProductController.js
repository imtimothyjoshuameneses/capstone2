const Product = require('../models/Product.js');
const bcrypt = require('bcrypt');
const auth = require('../auth.js');

// Create single product
module.exports.addProduct = (request, response) =>{
	let new_product = new Product({
		name: request.body.name,
		description: request.body.description,
		price: request.body.price,
		image: request.body.image,
		quantity: request.body.quantity
	});

	return new_product.save().then((saved_product, error) => {
		if(error){
			return response.send(false);
		}
		return response.send(true);
	}).catch(error => console.log(error));

}


// Get all products
module.exports.getAllProducts = (request, response) => {
	return Product.find({}).then(result => {
		return response.send(result);
	})
}

// Get all active products
module.exports.getAllActiveProducts = (request, response) => {
	return Product.find({isActive: true}).then(result => {
		return response.send(result);
	})	
}

// Retrieve a Specific Product
module.exports.getProducts = (request, response) => {
	return Product.findById(request.params.id).then(result => {
		return response.send(result);
	})
}

// Update product information 
module.exports.updateProduct = (request, response) => {
	let updated_product_details = {
		name: request.body.name,
		description: request.body.description,
		price: request.body.price,
		image: request.body.image,
		quantity: request.body.quantity
	};

	return Product.findByIdAndUpdate(request.params.id, updated_product_details).then((updated_product, error) => {

		if(error){
			return response.send({
				message: error.message
			})
		}

		return response.send({
			message: 'Product has been updated Successfully'
		})
	}).catch(error => console.log(error));
}


// Archive Product
module.exports.archiveProduct = (request, response) => {
	return Product.findByIdAndUpdate(request.params.id, {isActive: 'false'},{new: true}).then((product_archived, error) => {

		if(error) {
			return response.send({
				message: error.message
			})
		}
		return response.send({
			message: 'Product has been archived'
		})
	}).catch(error => console.log(error));
}


// Activate Product
module.exports.activateProduct = (request, response) => {
	return Product.findByIdAndUpdate(request.params.id, {isActive: "true"},{new: true}).then((product_activated, error) => {

		if(error){
			return response.send({
				message: error.message
			})
		}
		return response.send({
			message: 'Product has been activated'
		})
	}).catch(error => console.log(error));
}