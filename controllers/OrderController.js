const Order = require('../models/Order.js');
const bcrypt = require('bcrypt');
const auth = require('../auth.js');
const Product = require('../models/Product.js');
const User = require('../models/User.js');


// Create Order
module.exports.createOrder = async (request, response) => {
  // Block this function if the user is an admin
  if (request.user.isAdmin) {
      return response.send('Action Forbidden');
  }

  const { products } = request.body;
  const userId = request.user.id;

  // Fetch user's first name based on userId
  try {
      const user = await User.findById(userId);
      if (!user) {
          return response.send({ message: 'User not found' });
      }
      const userName = user.firstName + ` ` + user.lastName;
      const userMobileNo = user.mobileNo;

      // Calculate total amount based on the products and their quantities
      let totalAmount = 0;
      const orderedProducts = [];
      const updatedProductQuantities = []; // To track updated product quantities

      for (const product of products) {
          const productData = await Product.findById(product.productId);
          if (!productData) {
              return response.send({ message: `Product with ID ${product.productId} not found` });
          }

          if (!productData.price || !product.quantity) {
              return response.send({ message: `Invalid product data for ID ${product.productId}` });
          }

          if (product.quantity > productData.quantity) {
              return response.send({ message: 'Not enough stocks' });
          }

          totalAmount += productData.price * product.quantity;

          // Include product ID, product name, and quantity in orderedProducts array
          orderedProducts.push({
              productId: product.productId,
              productName: productData.name, // Fetch the product name from productData
              productPrice: productData.price,
              quantity: product.quantity,
          });

          // Decrease the product quantity and check if it's archived
          productData.quantity -= product.quantity;
          if (productData.quantity === 0) {
              productData.isActive = false;
          }
          updatedProductQuantities.push(productData);
      }

      // Update product quantities in the database
      await Promise.all(updatedProductQuantities.map(async updatedProduct => {
          await updatedProduct.save();
      }));

      // Create the order
      const new_order = new Order({
          userId: userId,
          userName: userName,
          userMobileNo: userMobileNo,
          products: orderedProducts,
          totalAmount: totalAmount,
      });

      new_order.save()
          .then(saved_order => {
              return response.send({
                  message: 'Order created successfully',
              });
          })
          .catch(error => {
              return response.send({
                  message: error.message,
              });
          });
  } catch (error) {
      console.error('Error fetching user data:', error);
      return response.send({
          message: 'Error fetching user data',
      });
  }
};

// Create order from user's cart
module.exports.createOrderFromCart = async (request, response) => {
  // Block this function if the user is an admin
  if (request.user.isAdmin) {
      return response.send('Action Forbidden');
  }

  const userId = request.user.id;

  try {
      // Fetch user's cart from the database
      const user = await User.findById(userId).populate('cart.productId');

      if (!user) {
          return response.send({ message: 'User not found' });
      }

      const cartProducts = user.cart;

      if (!cartProducts || cartProducts.length === 0) {
          return response.send({ message: 'Cart is empty' });
      }

      const updatedProductQuantities = []; // To track updated product quantities

      // Calculate total amount based on the cart products and their quantities
      let totalAmount = 0;
      const orderProducts = [];

      for (const cartProduct of cartProducts) {
          const productData = cartProduct.productId;

          if (!productData.price || !cartProduct.quantity) {
              return response.send({ message: `Invalid product data for ID ${productData._id}` });
          }

          const orderedQuantity = cartProduct.quantity;

          if (orderedQuantity > productData.quantity) {
              return response.send({ message: 'Not enough stocks' });
          }

          totalAmount += productData.price * orderedQuantity;
          orderProducts.push({
              productId: productData._id,
              productName: productData.name,
              productPrice: productData.price,
              quantity: orderedQuantity
          });

          // Decrease the product quantity and check if it's archived
          productData.quantity -= orderedQuantity;
          if (productData.quantity === 0) {
              productData.isActive = false;
          }
          updatedProductQuantities.push(productData);
      }

      // Update product quantities in the database
      await Promise.all(updatedProductQuantities.map(async updatedProduct => {
          await updatedProduct.save();
      }));

      // Create the order
      const userName = user.firstName + ` ` + user.lastName;
      const userMobileNo = user.mobileNo;
      
      const new_order = new Order({
          userId: userId,
          userName: userName,
          userMobileNo: userMobileNo,
          products: orderProducts,
          totalAmount: totalAmount,
      });

      const savedOrder = await new_order.save();

      // Clear user's cart after creating the order
      user.cart = [];
      await user.save();

      return response.send({
          message: 'Order created successfully',
          order: savedOrder,
      });
  } catch (error) {
      console.error('Error creating order from cart:', error);
      return response.send({
          message: 'Error creating order from cart',
      });
  }
};



// Retrieve all orders'
module.exports.getAllOrders = (request, response) => {
 Order.find({}).then(result => {
    return response.send(result);
  })
}

// Retrieve authenticated User's orders
module.exports.getUsersOrders = (request, response) => {
 Order.findById(request.params.id).then((orders) => {
    return response.send(orders);
  }).catch(error => console.log(error));
}