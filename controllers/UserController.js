const User = require('../models/User.js');
const Product = require('../models/Product.js');
const Order = require('../models/Order.js');
const bcrypt = require('bcrypt');
const auth = require('../auth.js');



module.exports.registerUser = async (request, response) => {
	
	const existingUser = await User.findOne({ email: request.body.email });
    if (existingUser) {
      return { message: 'Email address is already registered' };
    }

	let new_user =  new User({
		firstName: request.body.firstName,
		lastName: request.body.lastName,
		email: request.body.email,
		mobileNo: request.body.mobileNo,
		password: bcrypt.hashSync(request.body.password, 10)
	});

	return new_user.save().then((registered_user, error) => {
		if(error){
			return {
				message: error.message
			};		
		}

		return {
				message: 'Successfully registered a user!'
		};
	}).catch(error => console.log(error));
}


module.exports.loginUser = (request, response) => {
	return User.findOne({email: request.body.email}).then(result => {
		// Check if a user is found with an existing email
		if(result==null){
			return response.send({
				message: "The user isn't register yet."
			})
		}

		// If a user was found with an existing email, then check if the password of that user matched the input form the request body. 
		const isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password);

		if(isPasswordCorrect){
			// if teh password comparison returns true, then respond with the newly generated JWT access token.
			return response.send({
				accessToken: auth.createAccessToken(result),
				userId: result._id
			});
		} else {
			return response.send({
				message: 'your password is incorrect'
			})
		}
	}).catch(error => response.send(error));
} 

module.exports.getAllUsers = (request, response) => {
	return User.find({}).then(result => {
		return response.send(result);
	})
}

module.exports.getUserDetails = (request_body) => {
	return User.findOne({_id: request_body.id}).then((user, error) => {
		if(error){
			return {
				message: error.message
			}
		}

		user.password = "";
		return user;
	}).catch(error => console.log(error))
}

module.exports.setUserAsAdmin = (request, response) => {
	return User.findByIdAndUpdate(request.params.id,{isAdmin: "true"},{new:true}).then((result, error) => {

		if(error){
			return response.send({
				message: error.message
			})
		}

		return response.send(true)
	})
}


module.exports.removeUserAsAdmin = (request, response) => {
	return User.findByIdAndUpdate(request.params.id,{isAdmin:"false"},{new:true}).then((result, error) => {

		if(error){
			return response.send({
				message: error.message
			})
		}

		return response.send(false)
	})
}


module.exports.addToCart = async (request, response) => {
    const userId = request.user.id;
    const { productId, quantity } = request.body;

    const user = await User.findById(userId);
    if (!user) {
        return response.send({ message: 'User not found' });
    }

    const existingProductIndex = user.cart.findIndex(item => item.productId.toString() === productId);

    if (existingProductIndex !== -1) {
        // If the product is already in the cart, update quantity and total amount
        user.cart[existingProductIndex].quantity += quantity;
        user.cart[existingProductIndex].totalAmount = user.cart[existingProductIndex].productPrice * user.cart[existingProductIndex].quantity;
    } else {
        try {
            const product = await Product.findById(productId);
            if (!product) {
                return response.send({ message: 'Product not found' });
            }

            // Create the cart item using product details
            const cartItem = {
                productId,
				productImage: product.image,
                productName: product.name,
                productPrice: product.price,
                quantity,
                totalAmount: product.price * quantity,
            };
            user.cart.push(cartItem);
        } catch (error) {
            return response.send({ message: 'Error fetching product details' });
        }
    }

    // Recalculate overall total for the entire cart
    user.cartTotal = user.cart.reduce((total, item) => total + item.totalAmount, 0);

    return user.save()
        .then(saved_user => {
            return response.send({
                message: 'Product added to cart',
                cartTotal: saved_user.cartTotal,
            });
        })
        .catch(error => {
            return response.send({
                message: error.message
            });
        });
};

module.exports.updateCartItem = async (request, response) => {
    // Similar to addToCart but with updating quantity
		const userId = request.user.id;
		const { productId, quantity} = request.body;

		const user = await User.findById(userId);
			if(!user) {
			return response.send({
				message: 'User Not Found'
			})
		 }

		const existingCartItem = user.cart.find(item => item.productId.toString() === productId);

		if(!existingCartItem) {
			return response.send({
				message: 'Product not found in cart'
			});
		}

		// update the quantity of the existing cart item
		existingCartItem.quantity = quantity;

		return user.save().then((result, error) => {
			if(error){
				return response.send({
					message: error.message
				})
			}
			return response.send({
			messsage: 'Cart item udpated Successfully',
			updatedCartItem: existingCartItem
		})
	}).catch(error => console.log(error));
}


module.exports.removeFromCart = async (request, response) => {
    // Remove a product from the cart
	  const userId = request.user.id;
	  const productId = request.params.productId

	  const user = await User.findById(userId);

	  if(!user){
	  	return response.send({
	  		message: 'User not found!'
	  	})
	  }

	  // Find the idnex of the product in the cart
	  const productIndex = user.cart.findIndex(item => item.productId.toString() === productId);

	  if(productIndex !== -1){
	  	// remove the product from the cart
	  	user.cart.splice(productIndex, 1);
	  	return user.save().then((result, error) => {
	  		if(error){
	  			return response.send({
	  				message: error.message
	  			})
	  		}
	  		return response.send({
            message: 'Product removed from cart'
        })
	  	}).catch(error => console.log(error))
	  }else {
	  	return response.send({
	  		message: 'Product not found in cart!'
	  	})
	  }
};


module.exports.getUserCart = async (request, response) => {
   const userId = request.user.id;

    const user = await User.findById(userId).populate('cart.productId');
    if (!user) {
        return response.status(404).send({ message: 'User not found' });
    }

    const cartWithSubtotals = user.cart.map(item => ({
        product: item.productId,
        quantity: item.quantity,
        subtotal: item.quantity * item.productId.price,
    }));

    const total = cartWithSubtotals.reduce((acc, item) => acc + item.subtotal, 0);
    
    	return response.send({ 
        cart: cartWithSubtotals, 
        total 
    })
};
