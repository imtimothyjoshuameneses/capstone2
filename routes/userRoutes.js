const express = require('express');
const router = express.Router();
const UserController = require('../controllers/UserController.js');
const auth = require('../auth.js');

// You can destructure the 'auth' variable to extract the function being exported from it. You canthe use the functions directly without having to use dot(.) notation
// const {verify, verifyAdmin} = auth;

// INSERT ROUTES HERE

//  Register User
router.post('/register', (request, response) => {
	UserController.registerUser(request, response).then((result) =>{
		response.send(result);
	})
})


// Authenticate User / Login 
router.post('/login', (request, response) => {
	UserController.loginUser(request, response);
})

// Get all user
router.get('/all', (request, response) => {
	UserController.getAllUsers(request, response);
})

// get user details
router.post('/details', auth.verify,(request, response) => {
	UserController.getUserDetails(request.body).then((result) => {
		response.send(result);
	});
})

// Set User as Admin 
router.put('/:id/AsAdmin', auth.verify, auth.verifyAdmin,(request, response) => {
	UserController.setUserAsAdmin(request, response);
})

// Remove user as Admin
router.put('/:id/RemoveAsAdmin', auth.verify, auth.verifyAdmin,(request, response) => {
	UserController.removeUserAsAdmin(request, response);
})

// Add product to cart
router.post('/add-to-cart', auth.verify,(request, response) => {
	UserController.addToCart(request, response);
})

// Update cart item quantity
router.put('/update-cart-item', auth.verify, (request, response) => {
	UserController.updateCartItem(request, response);
})


// Remove product from cart
router.delete('/remove-from-cart/:productId', auth.verify, UserController.removeFromCart);


// Get user's cart and total price
router.get('/cart/:id', auth.verify, (request, response) => {
	UserController.getUserCart(request, response);
});

module.exports = router; 