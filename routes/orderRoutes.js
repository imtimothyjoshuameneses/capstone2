const express = require('express');
const router = express.Router();
const OrderController = require('../controllers/OrderController.js');
const auth = require('../auth.js');

// You can destructure the 'auth' variable to extract the function being exported from it. You canthe use the functions directly without having to use dot(.) notation
// const {verify, verifyAdmin} = auth;


// INSERT ROUTES HERE

// Create Order
router.post('/create-order', auth.verify, (request, response) => {
  OrderController.createOrder(request, response);
});

// Create order from user's cart
router.post('/create-order-cart', auth.verify, (request, response) => {
  OrderController.createOrderFromCart(request, response);
});

// Retrieve all orders
router.get('/all', (request, response) =>{
  OrderController.getAllOrders(request, response);
})


// Retrieve authenticated User's orders
router.get('/:id/orders', auth.verify, (request, response) => {
  OrderController.getUsersOrders(request, response);
})
module.exports = router;