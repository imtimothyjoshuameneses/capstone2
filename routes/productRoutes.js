const express = require('express');
const router = express.Router();
const ProductController = require('../controllers/ProductController.js');
const auth = require('../auth.js');

// You can destructure the 'auth' variable to extract the function being exported from it. You canthe use the functions directly without having to use dot(.) notation
// const {verify, verifyAdmin} = auth;


// INSERT ROUTES HERE

// Create product
router.post('/', auth.verify, auth.verifyAdmin, (request, response) => {
	ProductController.addProduct(request, response)

});

// Retrieve all products
router.get('/all', (request, response) => {
	ProductController.getAllProducts(request, response);
})

// Retrieve all active products
router.get('/', (request, response) => {
	ProductController.getAllActiveProducts(request, response);
})

// Retrieve a Specific Product
router.get('/:id', (request, response) =>{
	ProductController.getProducts(request, response);
})

// Update Product Information 
router.put('/:id', auth.verify, auth.verifyAdmin, (request, response) => {
	ProductController.updateProduct(request, response);
})

// Archive product
router.put('/:id/archive', auth.verify, auth.verifyAdmin, (request, response) => {
	ProductController.archiveProduct(request, response);

})


// Activate Product 
router.put('/:id/activate', auth.verify, auth.verifyAdmin, (request, response) => {
	ProductController.activateProduct(request, response);
})

module.exports = router;