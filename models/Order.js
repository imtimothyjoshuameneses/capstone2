const mongoose = require('mongoose');

// Schema
const order_schema = new mongoose.Schema({
	userId : {
        type : String,
        // Requires the data for this our fields/properties to be included when creating a record.
        // The "true" value defines if the field is required or not and the second element in the array is the message that will be printed out in our terminal when the data is not present
        required : [true, "UserId is required"]
	    },
    userName : {
        type: String,
        required: [true, "User name is Required"]
    },
    userMobileNo : {
        type: Number,
        required: [true, "User name is Required"]
    },
    products : [
        {
            productId : {
                type : String,
                required: [true, "ProductId is required"]
            },
            productName :{
                type : String,
                required: [true, "Product Name is required"]
            },
            productPrice :{
                type : Number,
                required: [true, "Product Price is required"]
            },
            quantity : {
                type : Number,
                required: [true, "quantity is required"],
                default: 1
            }
        }
    ],
    totalAmount : {
        type: Number,
        required: [true, "Total Amount is Required!"]
    },
    purchasedOn : {
        type: Date,
        default : new Date() 
    }
    
});


module.exports = mongoose.model("Order", order_schema);