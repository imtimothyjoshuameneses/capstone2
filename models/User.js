const mongoose = require('mongoose');

// Schema
const user_schema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, "First Name is Required"]
	},
	lastName: {
		type: String,
		required: [true, "Last Name is Required"]
	},
	email: {
		type: String,
		required : [true, "email is required"]
	},
	password: {
		type: String,
		required: [true, "Password is required"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	mobileNo: {
		type: Number,
		required: [true, "Mobile Number is Required"]
	},
	cart: [
		{
			productId : {
				type: mongoose.Schema.Types.ObjectId,
				ref: 'Product'
			},
			productName: {
				type: String,
				required: [true, "Product Name is Required"]
			},
			productPrice: {
				type: Number,
				required: [true, "Product Price is Required"]
			},
			quantity: {
				type: Number,
				default: 1,
			},
			totalAmount: {
				type: Number,
				required: true
			}
		}
	]
});


module.exports = mongoose.model("User", user_schema);