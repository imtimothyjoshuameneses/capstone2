// Server Variable
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
require('dotenv').config()
const port = process.env.PORT || 4000;
const userRoutes = require('./routes/userRoutes.js');
const productRoutes = require('./routes/productRoutes.js');
const orderRoutes = require('./routes/orderRoutes.js');
const multer = require('multer');
const app = express();


// middleware
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors()); //This will allow our hosted front-end app to send request to this server

// Configure multer to handle file uploads
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'uploads/'); // Specify where to store the uploaded files
  },
  filename: function (req, file, cb) {
    const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9);
    const extension = path.extname(file.originalname);
    cb(null, file.fieldname + '-' + uniqueSuffix + extension);
  },
});

const upload = multer({ storage });

app.post('/api/addProduct', upload.single('image'), (req, res) => {
  // Process the form data, including req.body and req.file (the uploaded image)
  const { name, price, description } = req.body;
  const imagePath = req.file.path; // This is the path where the uploaded image is stored on the server

  // Save product details and image path to your database
  // ...

  return res.json({ message: 'Product added successfully' });
});


// routes
app.use('/api/users', userRoutes);
app.use('/api/products', productRoutes);
app.use('/api/orders', orderRoutes);


// Database connection
mongoose.connect(`${process.env.MONGODB_CONNECTION_STRING}`, {
	useNewUrlParser: true,
	useUnifiedTopology: true
});


mongoose.connection.on('error', () => console.log("Can't connect to database"));
mongoose.connection.once('open', () => console.log("Connected to MongoDB!"));



app.listen(process.env.PORT || port, () => {
	console.log(`Capstone 2 - Ecommerce API is now running at localhost: ${process.env.PORT || port}`);
})

module.exports = app;